extern crate postgres;
use std::process;
use std::env;
use postgres::{Client, NoTls};

struct Config {
    host: String,
    username: String,
    password: String,
    database: String,
    port: i32
}

#[derive(Debug)]
struct StudentData {
    fio: String,
    subject: String,
    mark: i16,
}

fn load_env() -> Config {
    let host = env::var("DB_HOST").unwrap_or(String::from("localhost"));
    let username = env::var("DB_USERNAME").unwrap_or(String::from("postgres"));
    let password = env::var("DB_PASSWORD").unwrap_or(String::from("postgres"));
    let database = env::var("DB_NAME").unwrap_or(String::from("postgres"));
    let port: i32 = env::var("DB_PORT").unwrap_or(String::from("5432")).parse().unwrap();
    return Config { host, username, password, database, port };
}

fn main() {
    let config = load_env();
    let conn_string = format!("postgresql://{}:{}@{}:{}/{}", config.username, config.password, config.host, config.port, config.database);
    let mut client = Client::connect(conn_string.as_str(), NoTls).unwrap_or_else(|error| {
        eprintln!("Произошла ошибка при подключении: {}", error.to_string());
        process::exit(1);
    });

    let stmt = client.prepare(
        "SELECT s.name AS ФИО, m.subject AS Предмет, m.mark AS Оценка \
         FROM students s \
         JOIN markbooks m ON s.id = m.student_id \
         WHERE m.mark = (SELECT MIN(mark) FROM markbooks);"
    ).unwrap_or_else(|error: postgres::Error| {
        if let Some(db_error) = error.as_db_error() {
            eprintln!("Неправильно составлен запрос. Ответ от БД: {}", db_error.message());
            process::exit(1);
        } else {
            panic!("Произошла неизвестная ошибка")
        }
    });

    let rows = client.query(&stmt, &[]).unwrap();

    let mut student_data: Vec<StudentData> = Vec::new();
    for row in &rows {
        let fio: String = row.get("ФИО");
        let subject: String = row.get("Предмет");
        let mark: i16 = row.get("Оценка");

        let data = StudentData { fio, subject, mark };
        student_data.push(data);
    }

    for data in &student_data {
        println!("ФИО: {}, Предмет: {}, Оценка: {}", data.fio, data.subject, data.mark);
    }
}
