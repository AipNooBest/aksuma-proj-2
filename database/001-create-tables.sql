CREATE TABLE students (
    id SERIAL PRIMARY KEY,
    name TEXT,
    birth_date DATE
);

CREATE TABLE markbooks (
    id SERIAL PRIMARY KEY,
    student_id INTEGER REFERENCES students(id),
    subject TEXT,
    teacher TEXT,
    mark SMALLINT,
    exam_date DATE
);
