FROM rust:1.74 as builder

WORKDIR /build
RUN rustup target add x86_64-unknown-linux-musl
RUN rustup toolchain install stable-x86_64-unknown-linux-musl
COPY Cargo.lock Cargo.toml ./
COPY src src
RUN cargo build --release --target x86_64-unknown-linux-musl

FROM scratch
COPY --from=builder /build/target/x86_64-unknown-linux-musl/release/task2 /
CMD [ "/task2" ]